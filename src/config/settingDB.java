package config;
//zzz
import java.io.File;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class settingDB
{
  static String host;
  static String user;
  static String pass;
  static String port;
  static String db;
  static String dirname;
  static String host2;
  static String user2;
  static String pass2;
  static String port2;
  static String db2;
  
  public settingDB()
  {
    try
    {
      File fXmlFile = new File("config/setting.xml");
      DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
      DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
      Document doc = dBuilder.parse(fXmlFile);
      doc.getDocumentElement().normalize();
      NodeList nList = doc.getElementsByTagName("menu");
      Element eElement = null;
      
      Node nNode = nList.item(0);
      if (nNode.getNodeType() == 1)
      {
        eElement = (Element)nNode;
        host = eElement.getElementsByTagName("host").item(0).getTextContent();
        user = eElement.getElementsByTagName("user").item(0).getTextContent();
        pass = eElement.getElementsByTagName("pass").item(0).getTextContent();
        port = eElement.getElementsByTagName("port").item(0).getTextContent();
        db = eElement.getElementsByTagName("db").item(0).getTextContent();
      }
      nNode = nList.item(1);
      if (nNode.getNodeType() == 1)
      {
        eElement = (Element)nNode;
        host2 = eElement.getElementsByTagName("host").item(0).getTextContent();
        user2 = eElement.getElementsByTagName("user").item(0).getTextContent();
        pass2 = eElement.getElementsByTagName("pass").item(0).getTextContent();
        port2 = eElement.getElementsByTagName("port").item(0).getTextContent();
        db2 = eElement.getElementsByTagName("db").item(0).getTextContent();
      }
      nNode = nList.item(2);
      if (nNode.getNodeType() == 1)
      {
        eElement = (Element)nNode;
        dirname = eElement.getElementsByTagName("dirname").item(0).getTextContent();
      }
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }
  
  public String getDbURL()
  {
    String url = "jdbc:mysql://" + host + ":" + port + "/" + db;
    return url;
  }
  
  public String getDbURL2()
  {
    String url = "jdbc:mysql://" + host2 + ":" + port2 + "/";
    
    return url;
  }
  
  public String getUser()
  {
    return user;
  }
  
  public String getDirname()
  {
    String path = dirname + "mbp_tdr.log.";
    return path;
  }
  
  public String getPass()
  {
    return pass;
  }
  
  public String getUser2()
  {
    return user2;
  }
  
  public String getPass2()
  {
    return pass2;
  }
}
