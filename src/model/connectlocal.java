package model;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author mulher
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import config.settingDB;
import config.settingLog;

public class connectlocal {
    Connection con = null;
    Statement st = null;
    ResultSet rs = null;
    
    private String area = "Unknown";
    private String regional = "Unknown";
    private String branch = "Unknown";
    private String sub_branch = "Unknown";
    private String cluster = "Unknown";
    
    settingDB setdb;
    settingLog setlog;
    //xxx;
    public void connect(){
        /*String url = "jdbc:mysql://localhost:3306/smp";
        String user = "root";
        String password = "root";
        try {
            
            con = DriverManager.getConnection(url, user, password);
            st = con.createStatement();
            con.setAutoCommit(false);
        } catch (SQLException ex) {
        }*/
        this.setdb = new settingDB();
        String url = this.setdb.getDbURL();
        String user = this.setdb.getUser();
        String password = this.setdb.getPass();
        this.setlog = new settingLog();
        try
        {
          this.con = DriverManager.getConnection(url, user, password);
          this.st = this.con.createStatement();
          this.con.setAutoCommit(false);
        }
        catch (SQLException e)
        {
          this.setlog.writeLog(e.toString());
          this.setlog.closeLog();
        }
    }
    
    public void close(){
        try {
                con.commit();
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex);
            }
    }
    
    
    public boolean insert_bin_rpt(String DATE,String AREA,String REGION,String BRANCH,String SUB_BRANCH,String CLUSTER,String PARTNER_NAME,String ADN,String SERVICE_NAME,String TRX_TYPE,String LAC,String CI,String REVENUE,String TRANSACTION,String USER){
//        System.out.println("DATE :"+DATE);
        String nmtb = DATE.substring(5, 7);
        try {
//            st.executeUpdate("INSERT INTO `BIN_RPT_SUMMARY_" + nmtb +"`" 
            st.executeUpdate("INSERT INTO `BIN_RPT_SUMMARY_00`" // for testing only
//            st.executeUpdate("INSERT INTO `BIN_RPT_SUMMARY_00` " 
                    + "(`TRX_DATE`, `AREA`, `REGION`, `BRANCH`, `SUB_BRANCH`, "
                    + "`CLUSTER`, `PARTNER_NAME`, `ADN_NUMBER`, `SERVICE_NAME`, `SERVICE_TYPE`, "
                    //+ "`LAC`, `CI`, 
                    + "`REVENUE`, `TRAFFIC`, `USER`) "
                    + "VALUES ('"+DATE+"','"+AREA+"',"
                    + "'"+REGION+"','"+BRANCH+"','"+SUB_BRANCH+"',"
                    + "'"+CLUSTER+"','"+PARTNER_NAME+"','"+ADN+"','"+SERVICE_NAME+"','"+TRX_TYPE
                    //+ "'"+LAC+"','"+CI
                    +"','"+REVENUE+"','"+TRANSACTION+"','"+USER+"');");
            //System.out.println("INSERT INTO tes VALUES("+id+", '"+text+"')");
            return true;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return false;
    }
    
    public String check_lacci(String LAC,String CELLID) throws SQLException{
        String result = "";
        String[] arr = null;
        String[] listx = {"AREA","REGIONAL","BRANCH","SUB_BRANCH","CLUSTER"};
        String sql = "select * from bin_lacima WHERE LAC = '"+LAC+"' and CELL_ID = '"+CELLID+"'";
        //System.out.println(sql);
        ResultSetMetaData md;
        try {
            rs = st.executeQuery(sql);
            while (rs.next())
             for (int x = 0; x < listx.length;){
                String data = rs.getString(listx[x]);
                arr = data.split("\n");
                for (int i =0; i < arr.length; i++){
                    result=arr[i]+","+result;
                    switch(x){
                        case 0:setArea(arr[i]);
                        break;
                        case 1:setRegional(arr[i]);
                        break;
                        case 2:setBranch(arr[i]);
                        break;
                        case 3:setSub_branch(arr[i]);
                        break;
                        case 4:setCluster(arr[i]);
                        break;
                    }    
                }
                x++;
            }
            //System.out.println("result = " + result);
        }
        catch (Exception e)
        {
             System.out.println("ex="+e.getStackTrace());
        }
        return result;
    }    

    public String get_lacci(String LAC,String CELLID) throws SQLException{
        String x="";
        x=check_lacci(LAC,CELLID);
        return x;
    }
    
    public String getArea() {
	return area;
    }
    public void setArea(String area) {
//        System.out.println("area = " + area);
        this.area = area;
    }
    
    public String getRegional() {
	return regional;
    }
    public void setRegional(String regional) {
//        System.out.println("regional = " + regional);
        this.regional = regional;
    }
    
    public String getBranch() {
	return branch;
    }
    public void setBranch(String branch) {
//        System.out.println("branch = " + branch);
        this.branch = branch;
    }
    
    public String getSub_branch() {
	return sub_branch;
    }
    public void setSub_branch(String sub_branch) {
//        System.out.println("sub_branch = " + sub_branch);
        this.sub_branch = sub_branch;
    }
    
    public String getCluster() {
	return cluster;
    }
    public void setCluster(String cluster) {
//        System.out.println("cluster = " + cluster);
        this.cluster = cluster;
    }
    
    public ResultSet s_location_lacci(){
        try {
            //rs = st.executeQuery("SELECT trf_id,trf_kh,trf_par,trf_sp,trf_as FROM mbp_config.bin_trfconf");
            rs = st.executeQuery(" SELECT concat(LAC,'|',CELL_ID),AREA,REGIONAL,BRANCH,SUB_BRANCH,CLUSTER FROM bin_lacima ");
            return rs;
        } catch (SQLException e) {
            System.out.println("error "+e.getStackTrace());
            this.setlog.writeLog(e.toString());
            this.setlog.closeLog();
        }
        return null;
    }
    
    public ResultSet s_location_lac(){
        try {
            //rs = st.executeQuery("SELECT trf_id,trf_kh,trf_par,trf_sp,trf_as FROM mbp_config.bin_trfconf");
            rs = st.executeQuery(" SELECT LAC,AREA,REGIONAL,BRANCH,SUB_BRANCH,CLUSTER FROM bin_lacima ");
            return rs;
        } catch (SQLException e) {
            System.out.println("error "+e.getStackTrace());
            this.setlog.writeLog(e.toString());
            this.setlog.closeLog();
        }
        return null;
    }
}
