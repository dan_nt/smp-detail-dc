/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author ag111
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import config.settingDB;
import config.settingLog;

public class connect {
    Connection con = null;
    Statement st = null;
    ResultSet rs = null;
    settingDB setdb;
    settingLog setlog;
  
    public connect(String dbname){
        /*String url = "jdbc:mysql://10.251.38.109:3306/"+dbname;
        String user = "mbp";
        String password = "mbp345";
        try {
            con = DriverManager.getConnection(url, user, password);
            st = con.createStatement();
        } catch (SQLException ex) {
        } finally { 
        }*/
        this.setdb = new settingDB();
        this.setlog = new settingLog();
        String url = this.setdb.getDbURL2() + dbname;
        String user = this.setdb.getUser2();
        String password = this.setdb.getPass2();
        this.setlog = new settingLog();
        try
        {
          this.con = DriverManager.getConnection(url, user, password);
          this.st = this.con.createStatement();
        }
        catch (SQLException e)
        {
          e = 

            e;this.setlog.writeLog(e.toString());this.setlog.closeLog();
        }
        finally {}
    }
    
    public void close(){
        try {
                if (this.rs != null) {
                    this.rs.close();
                }
                if (this.st != null) {
                    this.st.close();
                }
                if (this.con != null) {
                    this.con.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex);
            }
    }
    
    public ResultSet s_mbp_config_bin_trfconf(){
        try {
            //rs = st.executeQuery("SELECT trf_id,trf_kh,trf_par,trf_sp,trf_as FROM mbp_config.bin_trfconf");
            this.rs = this.st.executeQuery(" SELECT trf_id, CASE WHEN trf_kh = '' || trf_kh = null || trf_kh = 'null' THEN '0' ELSE trf_kh END as trf_kh, CASE WHEN trf_par = '' || trf_par = null || trf_par = 'null' THEN '0' ELSE trf_par END as trf_par, CASE WHEN trf_sp = '' || trf_sp = null || trf_sp = 'null' THEN '0' ELSE trf_sp END as trf_sp, CASE WHEN trf_as = '' || trf_as = null || trf_as = 'null' THEN '0' ELSE trf_as END as trf_as FROM bin_trfconf");
            return this.rs;
        } catch (SQLException e) {
            System.out.println(e);
            this.setlog.writeLog(e.toString());
            this.setlog.closeLog();
        }
        return null;
    }
    
    public ResultSet s_mbp_config_bin_parconf(){
        try {
            this.rs = this.st.executeQuery("SELECT * FROM mbp_config.bin_parconf;");
            return this.rs;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }
    
    public ResultSet s_mbp_config_bin_srvconf(){
        try {
            this.rs = this.st.executeQuery("SELECT * FROM bin_srvconf");
            return this.rs;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }
    
    public ResultSet s_mbp_report_bin_rpt_servtype(){
        try {
            this.rs = this.st.executeQuery("SELECT * FROM bin_rpt_servtype");
            return this.rs;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }
    
    
    
    
}
