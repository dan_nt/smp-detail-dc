/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templatdr
 * and open the template in the editor.
 */
package dctest;

/**
 *
 * @author ag111
 */
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class DCTestbpk {

    /**
     * @param args the command line arguments
     */
    
    private static final String FILENAME = "C:\\Users\\mulher\\Documents\\ftp\\mbp\\mbp_tdr.log.20170110";

    public static void main(String[] args) {
        double hapus = 0;
        BufferedReader br = null;
        FileReader fr = null;
        HashMap<String, String> data = new HashMap<String, String>();
        int hit = 0;
        try {
            fr = new FileReader(FILENAME);
            br = new BufferedReader(fr);
            String sCurrentLine;
            br = new BufferedReader(new FileReader(FILENAME));
                        String tdr[] = null;
                        String datetime[] = null;
                        String date = null;
                        String time = null;
            while ((sCurrentLine = br.readLine()) != null) {
                //System.out.println(sCurrentLine);
                                tdr = sCurrentLine.split("\\|", -1);
                                datetime = tdr[0].split("\\ ", -1); //tanggal
                                date = datetime[0];
                                time = datetime[1];
                                
                                if((tdr[11].equals("") || tdr[11].equals("2")) && !tdr[1].equals("70") && !tdr[1].equals("71")){
                                    if(date.equals("")){
                                       date = "0000-00-00"; 
                                    }
                                    if(time.equals("")){
                                       time = "00:00:00"; 
                                    }
                                    if(tdr[4].equals("")){
                                       tdr[4] = "Unknow"; 
                                    }
                                    if(tdr[8].equals("")){
                                       tdr[8] = "Telkomsel"; 
                                    }
                                    if(tdr[5].equals("")){
                                       tdr[5] = "Unknow"; 
                                    }
                                    if(tdr[9].equals("")){
                                       tdr[9] = "Unknow"; 
                                    }
                                    if(tdr[1].equals("")){
                                       tdr[1] = "Unknow"; 
                                    }
                                    //String groupname = date+"_"+time+"_a"+tdr[4].substring(3,6)+"_r"+tdr[4].substring(3,6)+"_b"+tdr[4].substring(3,6)+"_"+tdr[8]+"_"+tdr[5]+"_"+tdr[9]+"_"+tdr[1];
                                    String groupname = date+"_a"+tdr[4].substring(3,6)+"_r"+tdr[4].substring(3,6)+"_b"+tdr[4].substring(3,6)+"_"+tdr[8]+"_"+tdr[5]+"_"+tdr[9]+"_"+tdr[1];
                                    //System.out.println(groupname);
                                    data.put(groupname + "_01",date);
                                    //data.put(groupname + "_02",time);
                                    data.put(groupname + "_03",tdr[4].substring(3,9));//dikoneksikan dengan tdr_msisdn
                                    data.put(groupname + "_04",tdr[4].substring(3,9));//dikoneksikan dengan tdr_msisdn
                                    data.put(groupname + "_05",tdr[4].substring(3,9));//dikoneksikan dengan tdr_msisdn
                                    data.put(groupname + "_06",tdr[4].substring(3,9));//dikoneksikan dengan tdr_msisdn
                                    data.put(groupname + "_07",tdr[4].substring(3,9));//dikoneksikan dengan tdr_msisdn
                                    data.put(groupname + "_08",tdr[8]); //partner id
                                    data.put(groupname + "_09",tdr[5]); //adn
                                    data.put(groupname + "_10",tdr[9]); //service_id
                                    data.put(groupname + "_11",tdr[1]); //trx type
                                    data.put(groupname + "_12",tdr[3]); //revenue
                                    //transaksi
                                    if(data.get(groupname+"_13") == null){
                                        data.put(groupname+"_13", "1");
                                    }else{
                                        hit = Integer.parseInt(data.get(groupname+"_13"))+1;
                                        data.put(groupname+"_13", String.valueOf(hit));
                                    }
                                    //user
                                    data.put(groupname + "_14",tdr[4]);
//                                    
//                                    System.out.println("============================");
//                                    System.out.println(date+"_a"+tdr[4].substring(3,6)+"_r"+tdr[4].substring(3,6)+"_b"+tdr[4].substring(3,6)+"_"+tdr[8]+"_"+tdr[5]+"_"+tdr[9]+"_"+tdr[1]);
//                                    System.out.println("Tanggal:"+date);
//                                    System.out.println("Waktu:"+time);
//                                    System.out.println("Area:"+tdr[4].substring(3,6));//dikoneksikan dengan tdr_msisdn
//                                    System.out.println("Regional:"+tdr[4].substring(3,6));
//                                    System.out.println("Branch:"+tdr[4].substring(3,6));
//                                    System.out.println("Partner Name:"+tdr[8]); //partner id
//                                    System.out.println("TDR ADN:"+tdr[5]); //adn
//                                    System.out.println("Service Name:"+tdr[9]); //service_id
//                                    System.out.println("Trx Type:"+tdr[1]); //trx type
//                                    System.out.println("Revenue:"+tdr[3]);
//                                    System.out.println("Transaksi:"+tdr[4]);
//                                    System.out.println("User(Distinct):"+tdr[4]);
//                                    System.out.println("============================");
                                }
                                //System.out.println(hapus++);
            }
            //set value o
            Set set = data.entrySet();
            Iterator iterator = set.iterator();
            System.out.println("|date|area|region|branch|sub_branch|cluster|partner_id|adn|service_id|trx_type|revenue|transaksi|user");
            int i=1;
            Map<String, String> treeMap = new TreeMap<String, String>(data);
            for (String str : treeMap.keySet()) {
                System.out.print(data.get(str));
                System.out.print("|");
                if(i%13==0){
                    System.out.println();
                }
                i++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null)
                    br.close();

                if (fr != null)
                    fr.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }

        }

    }
    
}
