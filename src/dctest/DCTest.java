/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templatdr
 * and open the template in the editor.
 */
package dctest;

/**
 *
 * @author ag111
 */
import config.settingDB;
import config.settingLog;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.connect;
import model.connectlocal;

public class DCTest {

    /**
     * @param args the command line arguments
     */
    static connect cn = null;
    static connectlocal cnl = null;
    static ResultSet rs = null;
    static settingLog setlog;
    static settingDB setDB = new settingDB();
    private static final String FILENAME = setDB.getDirname();
//    private static final String FILENAME = "C:\\Users\\ag111\\Documents\\file server\\mbp_tdr.log.20180919.1juta";
//    private static final String FILENAME = "C:\\Users\\ag111\\Documents\\file server\\mbp_tdr.log.20180918.3.txt";

    public static void main(String[] args) throws SQLException {
        String area = null;
        double count_data = 0;
        double count_insert = 0;
        BufferedReader br = null;
        FileReader fr = null;
        setlog = new settingLog();
        setlog.writeLog("Start MBP DC Application");
        SimpleDateFormat format1 = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat format2 = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
        Calendar c = Calendar.getInstance();
        c.add(5, -1);
        System.out.println(format2.format(c.getTime()) +" Start MBP DC Application");
        
        HashMap<String, String> data = new HashMap<String, String>();
        HashMap<String, String> hitung_user = new HashMap<String, String>();
        HashMap<String, String> parname = new HashMap<String, String>();
        parname = s_parconf();
        HashMap<String, String> service = new HashMap<String, String>();
        service = s_srvconf();
        HashMap<String, String> trx_type = new HashMap<String, String>();
        trx_type = s_trx_type();
        HashMap<String, String> tarif = new HashMap<String, String>();
        tarif = s_tarif();
        HashMap<String, String> location_lacci = new HashMap<String, String>();
        location_lacci = s_location_lacci();
        HashMap<String, String> location_lac = new HashMap<String, String>();
        location_lac = s_location_lac();
        
        HashMap<String, String> arrlist = new HashMap<String, String>();
        String result;
//        System.out.println(parname.get("29"));
//        System.exit(0);
        int hit = 0;
        int hit_revenue = 0;
        int count_lacci = 0;
        int count_lac = 0;
        int count_unk = 0;
        try {
            fr = new FileReader(FILENAME+ format1.format(c.getTime()));
//            fr = new FileReader(FILENAME);
            br = new BufferedReader(fr);
            String sCurrentLine;
            br = new BufferedReader(new FileReader(FILENAME+ format1.format(c.getTime())));
//            br = new BufferedReader(new FileReader(FILENAME));
                        String tdr[] = null;
                        String datetime[] = null;
                        String date = null;
                        String time = null;
            System.out.println("Reading and processing file..");
            cnl = new connectlocal();
            cnl.connect();
            while ((sCurrentLine = br.readLine()) != null) {
//                System.out.println("CurrentLine = " +count_data);
                                count_data++;
                                tdr = sCurrentLine.split("\\|", -1);
                                datetime = tdr[0].split("\\ ", -1); //tanggal
                                date = datetime[0];
                                time = datetime[1];
                                //exclude DLR (13, 23) and STK (70,71)
                                if((tdr[11].equals("") || tdr[11].equals("2")) && !tdr[1].equals("70") && !tdr[1].equals("71")
                                        && !tdr[1].equals("13") && !tdr[1].equals("23")){
//                                if((tdr[11].equals("") || tdr[11].equals("2")) && !tdr[1].equals("70") && !tdr[1].equals("71")){
                                    if(date.equals("")){
                                       date = "0000-00-00"; 
                                    }
                                    if(time.equals("")){
                                       time = "00:00:00"; 
                                    }
                                    if(tdr[4].equals("")){
                                       tdr[4] = "Unknown"; 
                                    }
                                    if(tdr[8].equals("") || parname.get(tdr[8])==null){
                                       tdr[8] = "Telkomsel"; 
                                    }else{
                                        tdr[8] = parname.get(tdr[8]);   
                                    }
                                    if(tdr[5].equals("")){
                                       tdr[5] = "Unknown"; 
                                    }
                                    if(tdr[9].equals("") || service.get(tdr[9])==null){
                                       tdr[9] = "Unknown"; 
                                    }else{
                                        tdr[9] = service.get(tdr[9]);
                                    }
                                    if(tdr[1].equals("") || trx_type.get(tdr[1]) == null){
                                       tdr[1] = "Unknown"; 
                                    }else{
                                        tdr[1] = trx_type.get(tdr[1]) ;
                                    }
                                    //String groupname = date+"_"+time+"_a"+tdr[4].substring(3,6)+"_r"+tdr[4].substring(3,6)+"_b"+tdr[4].substring(3,6)+"_"+tdr[8]+"_"+tdr[5]+"_"+tdr[9]+"_"+tdr[1];
                                    String groupname = date+"_a"+tdr[4].substring(3,6)+"_r"+tdr[4].substring(3,6)+"_b"+tdr[4].substring(3,6)+"_"+tdr[8]+"_"+tdr[5]+"_"+tdr[9]+"_"+tdr[1];
                                    //System.out.println(groupname);
                                    data.put(groupname + "_01",date);
                                    //data.put(groupname + "_02",time);
//                                    data.put(groupname + "_03",tdr[4].substring(3,9));//dikoneksikan dengan tdr_msisdn
//                                    data.put(groupname + "_04",tdr[4].substring(3,9));//dikoneksikan dengan tdr_msisdn
//                                    data.put(groupname + "_05",tdr[4].substring(3,9));//dikoneksikan dengan tdr_msisdn
//                                    data.put(groupname + "_06",tdr[4].substring(3,9));//dikoneksikan dengan tdr_msisdn
//                                    data.put(groupname + "_07",tdr[4].substring(3,9));//dikoneksikan dengan tdr_msisdn
                                    
                                    //DAN add this to check NULL area where LAC is available.
//                                    if((tdr[18].equals("NULL") && !tdr[27].equals("NULL")) || ( tdr[18].equals("") && tdr[27].equals(""))){
//                                    if((tdr[18].equals("NULL") && (!tdr[27].equals("NULL")||tdr[27].equals("NULL"))) || ( tdr[18].equals("") && tdr[27].equals(""))){
                                    if((tdr[18].equals("NULL")) || ( tdr[18].equals(""))){
                                        if(location_lacci.get(tdr[27]+"|"+tdr[28]+"_area") != null)
                                        {
                                            count_lacci++;
//                                          System.out.println("--------------LACCI-----------");
                                            data.put(groupname + "_03",location_lacci.get(tdr[27]+"|"+tdr[28]+"_area"));
                                            data.put(groupname + "_04",location_lacci.get(tdr[27]+"|"+tdr[28]+"_regional"));
                                            data.put(groupname + "_05",location_lacci.get(tdr[27]+"|"+tdr[28]+"_branch"));
                                            data.put(groupname + "_06",location_lacci.get(tdr[27]+"|"+tdr[28]+"_sub_branch"));
                                            data.put(groupname + "_07",location_lacci.get(tdr[27]+"|"+tdr[28]+"_cluster"));
                                        }
                                        else 
                                            if(location_lac.get(tdr[27]+"_area")!=null){
                                                count_lac++;
//                                              System.out.println("--------------LAC-----------");
                                                data.put(groupname + "_03",location_lac.get(tdr[27]+"_area"));
                                                data.put(groupname + "_04",location_lac.get(tdr[27]+"_regional"));
                                                data.put(groupname + "_05",location_lac.get(tdr[27]+"_branch"));
                                                data.put(groupname + "_06",location_lac.get(tdr[27]+"_sub_branch"));
                                                data.put(groupname + "_07",location_lac.get(tdr[27]+"_cluster"));
                                            }
                                            else{
                                                count_unk++;
//                                              System.out.println("--------------UNKNOWN-----------");
                                                data.put(groupname + "_03","Unknown");
                                                data.put(groupname + "_04","Unknown");
                                                data.put(groupname + "_05","Unknown");
                                                data.put(groupname + "_06","Unknown");
                                                data.put(groupname + "_07","Unknown");
                                            }
                                    }
                                    else
                                    {
                                        data.put(groupname + "_03",tdr[18]);
                                        if(tdr[18]=="NULL")
                                                System.out.println("area:"+tdr[18]);
                                        data.put(groupname + "_04",tdr[19]);
                                        data.put(groupname + "_05",tdr[20]);
                                        data.put(groupname + "_06",tdr[21]);
                                        data.put(groupname + "_07",tdr[22]);
                                    }
                                    
                                    data.put(groupname + "_08",tdr[8]); //partner id its done
                                    data.put(groupname + "_09",tdr[5]); //adn
                                    data.put(groupname + "_10",tdr[9]); //service_id
                                    data.put(groupname + "_11",tdr[1]); //trx type
                                    //data.put(groupname + "_12",tarif.get(tdr[6]+"_trf_par")); //revenue
                                    //data.put(groupname + "_12",tarif.get(tdr[6]+"_trf_kh")); //revenue
                                    if( tarif.get(tdr[6]+"_trf_par") == null){
                                        tarif.put(tdr[6]+"_trf_par","0");
                                    }else if(tarif.get(tdr[6]+"_trf_par").equals("")){
                                        tarif.put(tdr[6]+"_trf_par","0");
                                    }
                                    if(data.get(groupname+"_12") == null){
                                        if(tdr[3].equals("KH")){
                                            if( tarif.get(tdr[6]+"_trf_kh")==null){
                                              tarif.put(tdr[6]+"_trf_kh","0"); 
                                            }
                                            hit_revenue = Integer.parseInt(tarif.get(tdr[6]+"_trf_kh"))+Integer.parseInt(tarif.get(tdr[6]+"_trf_par"));
                                            data.put(groupname + "_12",String.valueOf(hit_revenue));
                                        }else if(tdr[3].equals("SP")){
                                            if( tarif.get(tdr[6]+"_trf_sp")==null){
                                              tarif.put(tdr[6]+"_trf_sp","0"); 
                                            }
                                            hit_revenue = Integer.parseInt(tarif.get(tdr[6]+"_trf_sp"))+Integer.parseInt(tarif.get(tdr[6]+"_trf_par"));
                                            data.put(groupname + "_12",String.valueOf(hit_revenue));
                                        }else if(tdr[3].equals("AS")){
                                            if( tarif.get(tdr[6]+"_trf_as")==null){
                                              tarif.put(tdr[6]+"_trf_as","0"); 
                                            }
                                            hit_revenue = Integer.parseInt(tarif.get(tdr[6]+"_trf_as"))+Integer.parseInt(tarif.get(tdr[6]+"_trf_par"));
                                            data.put(groupname + "_12",String.valueOf(hit_revenue));
                                        }else{
                                            if( tarif.get(tdr[6]+"_trf_kh")==null){
                                              tarif.put(tdr[6]+"_trf_kh","0"); 
                                            }
                                            hit_revenue = Integer.parseInt(tarif.get(tdr[6]+"_trf_kh"))+Integer.parseInt(tarif.get(tdr[6]+"_trf_par"));
                                            data.put(groupname + "_12",String.valueOf(hit_revenue));
                                        }
                                    }else{
                                        if(tdr[3].equals("KH")){
                                            if( tarif.get(tdr[6]+"_trf_kh")==null){
                                              tarif.put(tdr[6]+"_trf_kh","0"); 
                                            }
                                            hit_revenue = Integer.parseInt(data.get(groupname + "_12"))+Integer.parseInt(tarif.get(tdr[6]+"_trf_kh"))+Integer.parseInt(tarif.get(tdr[6]+"_trf_par"));
                                            data.put(groupname + "_12",String.valueOf(hit_revenue));
                                        }else if(tdr[3].equals("SP")){
                                            if( tarif.get(tdr[6]+"_trf_sp")==null){
                                              tarif.put(tdr[6]+"_trf_sp","0"); 
                                            }
                                            hit_revenue = Integer.parseInt(data.get(groupname + "_12"))+ Integer.parseInt(tarif.get(tdr[6]+"_trf_sp"))+Integer.parseInt(tarif.get(tdr[6]+"_trf_par"));
                                            data.put(groupname + "_12",String.valueOf(hit_revenue));
                                        }else if(tdr[3].equals("AS")){
                                            if( tarif.get(tdr[6]+"_trf_as")==null){
                                              tarif.put(tdr[6]+"_trf_as","0"); 
                                            }
                                            hit_revenue = Integer.parseInt(data.get(groupname + "_12"))+ Integer.parseInt(tarif.get(tdr[6]+"_trf_as"))+Integer.parseInt(tarif.get(tdr[6]+"_trf_par"));
                                            data.put(groupname + "_12",String.valueOf(hit_revenue));
                                        }else{
                                            if( tarif.get(tdr[6]+"_trf_kh")==null){
                                              tarif.put(tdr[6]+"_trf_kh","0"); 
                                            }
                                            hit_revenue = Integer.parseInt(data.get(groupname + "_12"))+ Integer.parseInt(tarif.get(tdr[6]+"_trf_kh"))+Integer.parseInt(tarif.get(tdr[6]+"_trf_par"));
                                            data.put(groupname + "_12",String.valueOf(hit_revenue));
                                        }
                                    }
                                    //transaksi
                                    if(data.get(groupname+"_13") == null){
                                        data.put(groupname+"_13", "1");
                                    }else{
                                        hit = Integer.parseInt(data.get(groupname+"_13"))+1;
                                        data.put(groupname+"_13", String.valueOf(hit));
                                    }
                                    //user
                                    if(data.get(groupname+"_14") == null){
                                        data.put(groupname+"_14", "1");
                                        hitung_user.put(groupname+"_14_"+tdr[4], "1");
                                    }else{
                                        if(hitung_user.get(groupname+"_14_"+tdr[4]) == null){
                                            hit = Integer.parseInt(data.get(groupname+"_14"))+1;
                                            data.put(groupname+"_14", String.valueOf(hit));
                                            hitung_user.put(groupname+"_14_"+tdr[4],"1");
                                        }
                                    }
                                }
            }
            System.out.println("Inserting to db..");
            Set set = data.entrySet();
            Iterator iterator = set.iterator();
            //System.out.println("|date|area|region|branch|sub_branch|cluster|partner_id|adn|service_id|trx_type|revenue|transaksi|user");
            int i=1;
            Map<String, String> treeMap = new TreeMap<String, String>(data);
            //connectlocal cnl = new connectlocal();
            //cnl.connect();
            DateFormat df = new SimpleDateFormat("HH:mm:ss");
            Calendar calobj = Calendar.getInstance();
//            System.out.println(df.format(calobj.getTime()));
            for (String str : treeMap.keySet()) {
                //System.out.print(data.get(str));
                //System.out.print("|");
                arrlist.put(String.valueOf(i), data.get(str));
                if(i%13==0){
                    //System.out.print();
//                    cnl.insert_bin_rpt(arrlist.get("1"), String.valueOf(df.format(calobj.getTime())), 
//                            arrlist.get("2"), arrlist.get("3"), arrlist.get("4"), arrlist.get("5"), arrlist.get("6"), 
//                             arrlist.get("7"), arrlist.get("8"), arrlist.get("9"),arrlist.get("10"), "lac", "ci",
//                            arrlist.get("11"), arrlist.get("12"), arrlist.get("13"));
                    cnl.insert_bin_rpt(arrlist.get("1"),
                            arrlist.get("2"), arrlist.get("3"), arrlist.get("4"), arrlist.get("5"), arrlist.get("6"), 
                             arrlist.get("7"), arrlist.get("8"), arrlist.get("9"),arrlist.get("10"), "lac", "ci",
                            arrlist.get("11"), arrlist.get("12"), arrlist.get("13"));
                    count_insert++;
                    i=0;
                }
                i++;
            }
            cnl.close();
            System.out.println("-------TEST---------");
            System.out.println("Count data:"+count_data);
            System.out.println("Count insert:"+count_insert);
            System.out.println("Count lacci:"+count_lacci);
            System.out.println("Count lac:"+count_lac);
            System.out.println("Count unk:"+count_unk);
            Calendar d = Calendar.getInstance();
            System.out.println(format2.format(d.getTime())+" Success");
        } catch (IOException e) {
            e.printStackTrace();
        //} catch (SQLException ex) {
        //    Logger.getLogger(DCTest.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (br != null)
                    br.close();

                if (fr != null)
                    fr.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

    }
    
    public static HashMap<String, String> s_srvconf(){
        cn = new connect("mbp_config");
        rs = cn.s_mbp_config_bin_srvconf();
        HashMap<String, String> service = new HashMap<String, String>();
        try {
            while (rs.next()) {
                service.put(rs.getString(1), rs.getString(2));
            }
            return service;
        } catch (SQLException e) {
            System.out.println(e);
        }finally{
            cn.close();
        }
        return null;
    }
    
    public static HashMap<String, String> s_parconf(){
        cn = new connect("mbp_config");
        //System.out.println("connection open");
        rs = cn.s_mbp_config_bin_parconf();
        HashMap<String, String> parname = new HashMap<String, String>();
        try {
            while (rs.next()) {
                parname.put(rs.getString(1), rs.getString(2));
            }
            return parname;
        } catch (SQLException e) {
            System.out.println(e);
        }finally{
            cn.close();
        }
        return null;
    }
    
    public static HashMap<String, String> s_tarif(){
        cn = new connect("mbp_config");
        rs = cn.s_mbp_config_bin_trfconf();
        HashMap<String, String> tarif = new HashMap<String, String>();
        try {
            while (rs.next()) {
                //trf_id,trf_kh,trf_par,trf_sp,trf_as
                tarif.put(rs.getString(1)+"_trf_kh", rs.getString(2));
                tarif.put(rs.getString(1)+"_trf_par", rs.getString(3));
                tarif.put(rs.getString(1)+"_trf_sp", rs.getString(4));
                tarif.put(rs.getString(1)+"_trf_as", rs.getString(5));
            }
            return tarif;
        } catch (SQLException e) {
            System.out.println(e);
        }finally{
            cn.close();
        }
        return null;
    }
    
             
    public static HashMap<String, String> s_trx_type(){
        cn = new connect("mbp_report");
        rs = cn.s_mbp_report_bin_rpt_servtype();
        HashMap<String, String> trx_type = new HashMap<String, String>();
        try {
            while (rs.next()) {
                trx_type.put(rs.getString(1), rs.getString(3));
            }
            return trx_type;
        } catch (SQLException e) {
            System.out.println(e);
        }finally{
            cn.close();
        }
        return null;
    }
    
      public static HashMap<String, String> s_location_lacci(){
        cnl = new connectlocal();
        cnl.connect();
        rs = cnl.s_location_lacci();
        HashMap<String, String> location_lacci = new HashMap<String, String>();
        try {
            while (rs.next()) {
                location_lacci.put(rs.getString(1)+"_area", rs.getString(2));
                location_lacci.put(rs.getString(1)+"_regional", rs.getString(3));
                location_lacci.put(rs.getString(1)+"_branch", rs.getString(4));
                location_lacci.put(rs.getString(1)+"_sub_branch", rs.getString(5));
                location_lacci.put(rs.getString(1)+"_cluster", rs.getString(6));
            }
            return location_lacci;
        } catch (SQLException e) {
            System.out.println(e);
        }finally{
            cnl.close();
        }
        return null;
    }
      
      public static HashMap<String, String> s_location_lac(){
        cnl = new connectlocal();
        cnl.connect();
        rs = cnl.s_location_lac();
        HashMap<String, String> location_lac = new HashMap<String, String>();
        try {
            while (rs.next()) {
                location_lac.put(rs.getString(1)+"_area", rs.getString(2));
                location_lac.put(rs.getString(1)+"_regional", rs.getString(3));
                location_lac.put(rs.getString(1)+"_branch", rs.getString(4));
                location_lac.put(rs.getString(1)+"_sub_branch", rs.getString(5));
                location_lac.put(rs.getString(1)+"_cluster", rs.getString(6));
            }
            return location_lac;
        } catch (SQLException e) {
            System.out.println(e);
        }finally{
            cnl.close();
        }
        return null;
    }
    
    
    
}
